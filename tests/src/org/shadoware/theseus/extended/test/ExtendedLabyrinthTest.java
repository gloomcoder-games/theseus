package org.shadoware.theseus.extended.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.shadoware.theseus.extended.ExtendedLabyrinth;
import org.shadoware.theseus.extended.Figure;
import org.shadoware.theseus.extended.Room;

public class ExtendedLabyrinthTest {
	
	private ExtendedLabyrinth labyrinth;
	
	@Before
	public void init() {
		labyrinth = new ExtendedLabyrinth();
		labyrinth.roomSize(10.0f);
		labyrinth.wallsWidth(1.0f);
		labyrinth.generate(7);
	}
	
	@Test
	public void roomsGeneration() {
		Room[] rooms = labyrinth.rooms();
		assertTrue(rooms.length == 7);
		for (Room room : rooms) {
			assertTrue(room != null);
		}
	}
	
	@Test
	public void allowedPosition() {
		Figure figure = new Figure();
		figure.moveTo(0, 0);
		figure.radius(1.0f);
		assertTrue(labyrinth.isLocationAllowed(figure));
		figure.moveTo(1000.0f, 1000.0f);
		assertTrue(! labyrinth.isLocationAllowed(figure));
	}
	
	@Test
	public void allocate() {
		Figure figure = new Figure();
		figure.moveTo(10000, 10000);
		labyrinth.allocate(figure);
		assertTrue(labyrinth.isLocationAllowed(figure));
	}
}
