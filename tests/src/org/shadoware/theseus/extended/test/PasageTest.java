package org.shadoware.theseus.extended.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.shadoware.theseus.extended.Figure;
import org.shadoware.theseus.extended.Passage;

public class PasageTest {
	
	private Passage passage;
	
	@Before
	public void init() {
		passage = new Passage();
	}

	@Test
	public void open() {
		passage.open();
		assertTrue(passage.isOpened());
		assertTrue(! passage.isClosed());
	}
	
	@Test
	public void close() {
		passage.close();
		assertTrue(passage.isClosed());
		assertTrue(! passage.isOpened());
	}
	
	@Test
	public void contains() {
		Figure location = new Figure();
		location.moveTo(10.5f, -10.7f);
		passage.moveTo(10, -10);
		passage.size(2.0f);
		assertTrue(passage.contains(location));
	}
}
