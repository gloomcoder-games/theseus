package org.shadoware.theseus.extended.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.shadoware.theseus.extended.Figure;

public class FigureTest {
	
	@Test
	public void isIntersecting() {
		Figure a = new Figure();
		a.radius(1.0f);
		a.moveTo(10.0f, 10.0f);
		
		Figure b = new Figure();
		b.radius(1.0f);
		b.moveTo(8.0f, 10.0f);
		
		assertTrue(a.isIntersecting(b));	
	}
	
	@Test
	public void move() {
		Figure a = new Figure();
		a.moveTo(10.0f, -5.0f);
		assertTrue(a.x() == 10.0f);
		assertTrue(a.y() == -5.0f);
	}
	
	@Test
	public void radius() {
		Figure a = new Figure();
		a.radius(3.0f);
		assertTrue(a.radius() == 3.0f);
	}
}
