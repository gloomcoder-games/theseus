package org.shadoware.theseus.extended.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.shadoware.theseus.extended.Figure;
import org.shadoware.theseus.extended.NoPassageException;
import org.shadoware.theseus.extended.Room;
import org.shadoware.theseus.extended.Room.Wall;

public class RoomTest {
	
	private float roomSize = 10.0f;
	private float wallWidth = 1.0f;
	private float elementRadius = 1.0f;
	private float passageSize = 4.0f;
	
	private Room room;
	
	@Before
	public void init() {
		room = new Room(0, 0);
		room.dimension(roomSize, wallWidth);
	}
	
	@Test
	public void elementIn() {
		Figure in = new Figure();
		in.moveTo(-1.0f, 1.0f);
		in.radius(elementRadius);
		assertTrue(room.isAllowed(in));
	}
	
	@Test
	public void elementOut() {
		Figure out = new Figure();
		out.moveTo(4.5f, -4.5f);
		out.radius(elementRadius);
		assertTrue(! room.isAllowed(out));
	}
	
	@Test
	public void northPassage() {
		room.addPassage(Room.Wall.north, passageSize);
		Figure element = new Figure();
		element.radius(1.0f);
		
		element.moveTo(0.0f, 5.0f);
		assertTrue(room.isAllowed(element));
		
		element.moveTo(2.5f, 5.0f);
		assertTrue(! room.isAllowed(element));
	}
	
	@Test
	public void southPassage() {
		room.addPassage(Room.Wall.south, passageSize);
		Figure element = new Figure();
		element.radius(1.0f);
		
		element.moveTo(0.0f, -5.0f);
		assertTrue(room.isAllowed(element));
		
		element.moveTo(2.5f, -5.0f);
		assertTrue(! room.isAllowed(element));
	}
	
	@Test
	public void eastPassage() {
		room.addPassage(Room.Wall.east, passageSize);
		Figure element = new Figure();
		element.radius(1.0f);
		
		element.moveTo(5.0f, 0.0f);
		assertTrue(room.isAllowed(element));
		
		element.moveTo(5.0f, 2.5f);
		assertTrue(! room.isAllowed(element));
	}
	
	@Test
	public void westPassage() {
		room.addPassage(Room.Wall.west, passageSize);
		Figure element = new Figure();
		element.radius(1.0f);
		
		element.moveTo(-5.0f, 0.0f);
		assertTrue(room.isAllowed(element));
		
		element.moveTo(-5.0f, 2.5f);
		assertTrue(! room.isAllowed(element));
	}
	
	@Test
	public void openClosePassage() throws NoPassageException {
		room.addPassage(Wall.north, passageSize);
		
		room.closePassage(Wall.north);
		assertTrue(room.isClosed(Wall.north));
		
		room.openPassage(Wall.north);
		assertTrue(room.isOpened(Wall.north));
	}
	
	@Test
	public void openCloseAllPassages() throws NoPassageException {
		room.addPassage(Wall.north, passageSize);
		room.addPassage(Wall.south, passageSize);
		room.addPassage(Wall.east, passageSize);
		room.addPassage(Wall.west, passageSize);
		
		room.closeAllPassages();
		assertTrue(room.isClosed(Wall.north));
		assertTrue(room.isClosed(Wall.south));
		assertTrue(room.isClosed(Wall.east));
		assertTrue(room.isClosed(Wall.west));
		
		room.openAllPassages();
		assertTrue(room.isOpened(Wall.north));
		assertTrue(room.isOpened(Wall.south));
		assertTrue(room.isOpened(Wall.east));
		assertTrue(room.isOpened(Wall.west));
	}
	
	@Test
	public void elementInPassage() throws NoPassageException {
		Figure element = new Figure();
		element.radius(elementRadius);
		element.moveTo(0.0f, 5.0f);
		room.addPassage(Room.Wall.north, passageSize);
		
		room.openPassage(Wall.north);
		assertTrue(room.isAllowed(element));
		
		room.closePassage(Wall.north);
		assertTrue(! room.isAllowed(element));
	}
	
	@Test
	public void isPassage() {
		room.addPassage(Room.Wall.north, passageSize);
		assertTrue(room.isPassage(Wall.north));
		assertTrue(! room.isPassage(Wall.south));
	}
	
	@Test
	public void noPassage() {
		boolean noPassage = false;
		try {
			room.isOpened(Wall.north);
		} catch (NoPassageException e) {
			noPassage = true;
		}
		assertTrue(noPassage);
	}
	
	@Test
	public void location() {
		Room room = new Room(10.5f, -30.0f);
		assertTrue(room.x() == 10.5f);
		assertTrue(room.y() == -30.0f);
	}
	
	@Test
	public void size() {
		Room room = new Room(0, 0);
		room.dimension(3.0f, 2.5f);
		assertTrue(room.size() == 3.0f);
	}
	
	@Test
	public void containsFigure() {
		Figure figure = new Figure();
		assertTrue(room.contains(figure));
		
		float limit = roomSize / 2;
		figure.moveTo(limit, limit);
		assertTrue(room.contains(figure));
		
		limit += 0.5f;
		figure.moveTo(limit, limit);
		assertTrue(! room.contains(figure));
	}
	
	@Test
	public void allocate() {
		Figure figure = new Figure();
		figure.moveTo(100000, 100000);
		room.allocate(figure);
		assertTrue(room.isAllowed(figure));
	}
}
