package org.shadoware.theseus.extended;

import com.shadoware.athena.geometry.Square;

public class Passage extends Zone {
	
	private boolean opened = true;
	
	public Passage() {
		shape = new Square();
	}
	
	public void open() {
		opened = true;
	}
	
	public void close() {
		opened = false;
	}
	
	public boolean isOpened() {
		return opened;
	}
	
	public boolean isClosed() {
		return ! opened;
	}
	
	public void size(float size) {
		((Square)shape).side(size);
	}
 }
