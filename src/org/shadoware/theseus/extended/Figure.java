package org.shadoware.theseus.extended;

import com.shadoware.athena.geometry.Circle;

public class Figure {

	private Circle shape = new Circle();
	
	public void moveTo(float x, float y) {
		shape.moveTo(x, y);
	}
	
	public void radius(float radius) {
		shape.radius(radius);
	}
	
	public boolean isAllowed(Room room) {
		return room.isAllowed(this);
	}
	
	public float x() {
		return shape.x();
	}
	
	public float y() {
		return shape.y();
	}
	
	public float radius() {
		return shape.radius();
	}
	
	public boolean isIntersecting(Figure figure) {
		return shape.isIntersecting(figure.shape);
	}
}
