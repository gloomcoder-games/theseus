package org.shadoware.theseus.extended;

import com.shadoware.athena.geometry.Circle;
import com.shadoware.athena.geometry.Point;
import com.shadoware.athena.geometry.Square;

public class Room {
	
	public enum Wall {north, south, east, west};
	
	private final Square floor = new Square();
	private final Square walls = new Square();
	private final Passage[] passages = new Passage[Wall.values().length];
	
	public Room(float x, float y) {
		floor.moveTo(x, y);
		walls.moveTo(x, y);
	}
	
	public void dimension(float roomSize, float wallWidth) {
		walls.side(roomSize);
		floor.side(roomSize - (2 * wallWidth));
	}

	public boolean isAllowed(Figure location) {
		return isFigureInsideFloor(location) || isFigureInsideSomeOpenedPassage(location);
	}
	
	public void addPassage(Wall wall, float size) {
		Passage passage = new Passage();
		passage.size(size);
		updatePassagePosition(passage, wall);
		passages[wall.ordinal()] = passage;
	}
	
	public void openAllPassages() {
		for (Wall wall : Wall.values()) {
			try {
				openPassage(wall);
			} catch (NoPassageException e) {
			}
		}
	}
	
	public void closeAllPassages() {
		for (Wall wall : Wall.values()) {
			try {
				closePassage(wall);
			} catch (NoPassageException e) {
			}
		}
	}
	
	public void openPassage(Wall wall) throws NoPassageException {
		try {
			passages[wall.ordinal()].open();
		}
		catch (Exception e) {
			throw new NoPassageException();
		}
	}
	
	public void closePassage(Wall wall) throws NoPassageException {
		try {
			passages[wall.ordinal()].close();
		}
		catch (Exception e) {
			throw new NoPassageException();
		}
	}
	
	public boolean isOpened(Wall wall) throws NoPassageException {
		try {
			return passages[wall.ordinal()].isOpened();
		}
		catch (Exception e) {
			throw new NoPassageException();
		}
	}
	
	public boolean isClosed(Wall wall) throws NoPassageException {
		return ! isOpened(wall);
	}
	
	public boolean isPassage(Wall wall) {
		return (passages[wall.ordinal()] != null);
	}
	
	public void allocate(Figure figure) {
		do {
			figure.moveTo(randomCoordinate(this.x()), randomCoordinate(this.y()));
		}
		while (! isAllowed(figure));
	}
	
	private float randomCoordinate(float center) {
		float increment = (float) (walls.side() * (Math.random() - 0.5f));
		return increment + center;
	}
	
	public float x() {
		return floor.x();
	}
	
	public float y() {
		return floor.y();
	}
	
	public float size() {
		return walls.side();
	}
	
	public boolean contains(Figure figure) {
		Point figureCenter = new Point();
		figureCenter.moveTo(figure.x(), figure.y());
		return figureCenter.isInside(walls);
	}
	
	private void updatePassagePosition(Passage passage, Wall wall) {
		float xInc = 0;
		float yInc = 0;
		float side = walls.halfSide();
		switch (wall) {
		case north : yInc = side; break;
		case south : yInc = -side; break;
		case east : xInc = side; break;
		case west : xInc = -side; break;
		}
		passage.moveTo(floor.x() + xInc, floor.y() + yInc);
	}
	
	private boolean isFigureInsideFloor(Figure location) {
		Circle locationShape = new Circle();
		locationShape.moveTo(location.x(), location.y());
		locationShape.radius(location.radius());
		return locationShape.isInside(floor);
	}
	
	private boolean isFigureInsideSomeOpenedPassage(Figure figure) {
		for (Passage passage : passages) {
			if (isFigureInsideOpenedPassage(passage, figure)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isFigureInsideOpenedPassage(Passage passage, Figure location) {
		try {
			if (passage.isOpened() && passage.contains(location)) {
				return true;
			}
		}
		catch (Exception e) {
		}
		return false;
	}
}
