package org.shadoware.theseus.extended;

import com.shadoware.athena.geometry.Point;
import com.shadoware.athena.geometry.Shape;

public abstract class Zone {
	
	protected Shape shape;

	public void moveTo(float x, float y) {
		shape.moveTo(x, y);
	}
	
	public boolean contains(Figure figure) {
		Point figureShape = new Point();
		figureShape.moveTo(figure.x(), figure.y());
		return figureShape.isInside(shape);
	}
}
