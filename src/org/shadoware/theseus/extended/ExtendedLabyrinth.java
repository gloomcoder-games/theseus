package org.shadoware.theseus.extended;

import org.shadoware.theseus.Connector;
import org.shadoware.theseus.Node;
import org.shadoware.theseus.basic.BasicLabyrinth;
import org.shadoware.theseus.basic.BasicNode;
import org.shadoware.theseus.extended.Room.Wall;

public class ExtendedLabyrinth extends BasicLabyrinth {

	private float roomSize;
	private float wallsWidth;
	private float doorSize;
	private Room[] rooms;
	
	public void roomSize(float size) {
		this.roomSize = size;
	}
	
	public void wallsWidth(float width) {
		this.wallsWidth = width;
	}
	
	public void doorSize(float size) {
		this.doorSize = size;
	}
	
	@Override
	public void generate(int numberOfRooms) {
		super.generate(numberOfRooms);
		createRoomsFromNodes(this.nodes());
	}
	
	public boolean isLocationAllowed(Figure figure) {
		Room figureRoom;
		try {
			figureRoom = findFigureRoom(figure);
			return figureRoom.isAllowed(figure);
		} catch (FigureOutOfRoomsException e) {
			return false;
		}
	}
	
	public Room[] rooms() {
		if (rooms != null) {
			return rooms;
		}
		else {
			return new Room[0];
		}
	}
	
	public void allocate(Figure figure) {
		Room room = randomRoom();
		room.allocate(figure);
	}
	
	private Room randomRoom() {
		int index = (int) (Math.random() * rooms.length);
		return rooms[index];
	}
	
	private void createRoomsFromNodes(Node[] nodes) {
		int numberOfRooms = nodes.length;
		rooms = new Room[numberOfRooms];
		for (int i=0; i<numberOfRooms; i++) {
			rooms[i] = createRoom(nodes[i]);
		}
	}
	
	private Room createRoom(Node node) {
		float x = node.position().getX() * roomSize;
		float y = node.position().getY() * roomSize;
		Room room = new Room(x, y);
		room.dimension(roomSize, wallsWidth);
		addPassagesFromNode(node, room);
		return room;
	}
	
	private void addPassagesFromNode(Node node, Room room) {
		Connector[] connectors = node.connectors();
		for (int i=0; i<connectors.length; i++) {
			Connector connector = connectors[i];
			if (connector instanceof Node) {
				room.addPassage(getWallFromConnector(i), doorSize);
			}
		}
	}
	
	private Wall getWallFromConnector(int connector) {
		Wall wall;
		switch (connector) {
		case BasicNode.UP_CONNECTOR : wall = Wall.north; break;
		case BasicNode.RIGHT_CONNECTOR : wall = Wall.east; break;
		case BasicNode.DOWN_CONNECTOR : wall = Wall.south; break;
		case BasicNode.LEFT_CONNECTOR : wall = Wall.west; break;
		default : wall = Wall.north;
		}
		return wall;
	}
	
	private Room findFigureRoom(Figure figure) throws FigureOutOfRoomsException {
		for (Room room : rooms()) {
			if (room.contains(figure)) {
				return room;
			}
		}
		throw new FigureOutOfRoomsException();
	}
}
